import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import argrelextrema
from scipy.interpolate import make_interp_spline

print("---------- 交流电路功率因数实验 ----------")

plt.rc("font", family="SimSun", size=12, weight="bold")

Is = [0.2963, 0.2341, 0.1820, 0.1393, 0.1207, 0.1360, 0.1793, 0.2315, 0.2877, 0.3511]
Us = [218.9, 218.7, 218.8, 218.8, 218.9, 218.7, 219.9, 219.0, 218.9, 219.1]
Ps = [22.5, 23.0, 23.2, 23.2, 23.3, 23.3, 23.5, 23.5, 23.5, 23.6]
Cs = np.linspace(0, 9, 10)
cosphis = []

cosphis = list(map(lambda i, u, p: p / (u * i), Is, Us, Ps))

print("功率因数：", end="")
for cosphi in cosphis:
    print(round(cosphi, 3), end=" ")
print()

plt.figure(figsize=(8, 4))
plt.scatter(Cs, cosphis)
plt.title("cosφ-C 曲线", weight="bold", size=18)
plt.xlabel("C/μF", weight="bold", size=14)
plt.ylabel("cosφ", weight="bold", size=14)
plt.grid(True)
for c, cosphi in zip(Cs, cosphis):
    plt.text(c + 0.05, cosphi, f"({c}, {round(cosphi, 3)})", ha="left", va="top")

# model = np.polyfit(Cs, cosphis, 5)
# model_fn = np.poly1d(model)
model = make_interp_spline(Cs, cosphis)
Cs_model = np.arange(0, 9.5, 0.01)
index_C = argrelextrema(model(Cs_model), np.greater)[0] # find the indices of the extreme points, and get the first of these indices
max_C = round(float(Cs_model[index_C]), 3) # the x-cord of this index
max_cosphi = round(float(model(max_C)), 3) # tht y-cord of this index
print(f"图像最高点：({max_C}, {max_cosphi})")
plt.plot(Cs_model, model(Cs_model), color="orange")

plt.scatter([max_C], [max_cosphi], marker="x", color="black")
plt.text(max_C + 0.05, max_cosphi, f"({max_C}, {max_cosphi})", ha="left", va="bottom")

plt.ylim((0, 1))
plt.show()

print("\n---------- 弗兰克赫兹实验 ----------")

Ug2k = [
    [16.1,27.5,39.2,51.9,63.2,75.7,89.9],
    [16.0,27.4,39.4,52.1,62.9,75.8,89.7],
    [15.9,27.7,39.5,52.0,62.9,75.7,89.8],
    [15.8,27.5,39.1,51.9,63.1,76.0,90.1],
    [16.0,27.6,39.0,52.0,62.7,75.9,90.0]
]

U = [round(((e + f + g) - (a + b + c)) / 12, 1) for a, b, c, _, e, f, g in Ug2k]
U_ = round(sum(U) / len(U), 1)
uncertain = np.sqrt((1 / 20) * sum((u - U_) ** 2 for u in U))

print(f"逐差计算第一激发电势：{' '.join(map(str, U))}")
print(f"平均值：{U_}V")
print(f"相对误差：{round(abs(U_ - 11.61) / 11.61 * 100, 1)}%")
print(f"不确定度：{uncertain}V -> 0.1V")

"""res:
---------- 交流电路功率因数实验 ----------
功率因数：0.339 0.408 0.565 0.709 0.892 0.852 0.628 0.509 0.394 0.316 
图像最高点：(4.35, 0.91)

---------- 弗兰克赫兹实验 ----------
逐差计算第一激发电势：11.8 11.8 11.9 11.9 11.9
平均值：11.9V
相对误差：2.5%
不确定度：0.03162277660168368V -> 0.1V
"""
