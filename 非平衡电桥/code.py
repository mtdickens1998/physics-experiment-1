import numpy as np
import matplotlib.pyplot as plt

plt.rc("font", family="SimSun", size=13, weight="bold")
plt.figure(figsize=(13, 6))

t = [26.0, 36.0, 40.0, 45.0, 50.0, 55.0, 60.0, 65.0]
U = [34.2, 48.3, 53.3, 59.2, 65.2, 69.2, 74.3, 79.9]
alphas = []
E = 1.3
alpha_expect = 0.004280

for t_, U_ in zip(t, U):
    U_ = U_ / 1000
    alphas.append(4 * U_ / (t_ * (E - 2 * U_)))
alpha = sum(alphas) / len(alphas)

print(f"alpha values: {alphas}")
print(f"average alpha: {alpha} with relative error {abs(alpha - alpha_expect) / alpha_expect * 100}%")

plt.subplot(121)
plt.scatter(t, U)
plt.title("Cu50 电阻 U-t 特性曲线", weight="bold", size=20)
plt.xlabel("t/℃", weight="bold", size=16)
plt.ylabel("U/mV", weight="bold", size=16)
plt.grid(True)
for t_, U_ in zip(t, U):
    plt.text(t_ - 2, U_, f"({t_}, {U_})", ha="right", va="center")

linear_model = np.polyfit(t, U, 1)
print(f"linear fit model of U-t: U = {linear_model[0]} * t + {linear_model[1]}")
linear_model_fn = np.poly1d(linear_model)
x_s = np.arange(0, 70)
plt.ylim(0, 90)

plt.text(5, 5, f"U = {linear_model[0]:.3f} * t + {linear_model[1]:.3f}", color='red')
plt.plot(x_s, linear_model_fn(x_s), color="orange", linestyle="--")

# k = sum(x*y for x, y in zip(t, U)) / sum(x**2 for x in t)
# print(f"proportional fit model of U-t: U = {k} * t")
# print(
#     f"alpha calculated by proportional fit: {4 * k / 1000 / 1.3}"
#     f" with relative error {abs(4 * k / 1000 / 1.3 - alpha_expect) / alpha_expect * 100}%"
# )
# fn = np.poly1d([k, 0])
# plt.plot(x_s, fn(x_s), color="green")

t = [26.0, 36.0, 40.0, 45.0, 50.0, 55.0, 60.0, 65.0]
R = [55.70, 58.16, 58.87, 60.01, 60.83, 61.90, 62.97, 63.76]

plt.subplot(122)
plt.scatter(t, R)
plt.title("Cu50 电阻 Rt-t 特性曲线", weight="bold", size=20)
plt.xlabel("t/℃", weight="bold", size=16)
plt.ylabel("Rt/Ω", weight="bold", size=16)
plt.grid(True)
for t_, R_ in zip(t, R):
    plt.text(t_ - 2, R_, f"({t_}, {R_})", ha="right", va="center")

linear_model = np.polyfit(t, R, 1)
print(f"linear model of Rt-t: Rt = {linear_model[0]} * t + {linear_model[1]}")
linear_model_fn = np.poly1d(linear_model)
x_s = np.arange(0, 70)

plt.text(15, 52, f"Rt = {linear_model[0]:.3f} * t + {linear_model[1]:.3f} = \n {linear_model[1]:.3f}(1 + {linear_model[0] / linear_model[1] :.5f} * t)", color='red')
plt.text(15, 51, f"alpla = {linear_model[0] / linear_model[1] :.5f}", color='blue')
plt.plot(x_s, linear_model_fn(x_s), color="green")

R0 = linear_model[1]
R0_alpha = linear_model[0]
alpha_ = R0_alpha / R0
print(
    f"alpha calculated by Rt-t graph: {alpha_}"
    f" with relative error {abs(alpha_ - alpha_expect) / alpha_expect * 100}%"
)

plt.show()

"""
result:

alpha values: [0.004272116321482999, 0.004459586726497146, 0.004466230936819172, 0.0044534717520499515, 0.004459644322845418, 0.004332582018532431, 0.004302009148283248, 0.0043123338685520755]
average alpha: 0.004382246886882805 with relative error 2.388945955205732%
linear fit model of U-t: U = 1.1447867047740028 * t + 6.50192653752518
proportional fit model of U-t: U = 1.2741594975457855 * t
alpha calculated by proportional fit: 0.00392049076167934 with relative error 8.39974855889393%
linear model of Rt-t: Rt = 0.20504710490102698 * t + 50.61215518153911
alpha calculated by Rt-t graph: 0.004051341108979653 with relative error 5.342497453746415%
"""
